/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.databaseproject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author boat5
 */
public class SelectDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";

        //Connect Database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("complete");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        //Select Database
        String sql = "SELECT * FROM Category";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
             
            while(rs.next()){
                System.out.println(rs.getInt("category_id") + " " + 
                        rs.getString("category_name"));
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        //Close Database 
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}