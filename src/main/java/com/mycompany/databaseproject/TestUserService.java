/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;
import com.mycompany.databaseproject.dao.UserDao;
import com.mycompany.databaseproject.model.User;
/**
 *
 * @author boat5
 */
public class TestUserService {
    public static void main(String[] args){
        TestUserService userservice = new TestUserService();
        User user = userservice.login("user2", "pasa");
        if (user != null) {
            System.out.println("Welcome User : " + user.getName());
        } else {
            System.out.println("Error");
        }
    }

    public User login(String name, String password){
        UserDao userdao = new UserDao();
        User user = userdao.getByName(name);
        if(user != null && user.getPassword().equals(password)){
            return user; 
        }
        return null;
    }
}
